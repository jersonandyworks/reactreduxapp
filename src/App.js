import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
// import List from "./components/List";
// import Form from "./components/Forms";
import OrderForm from "./components/OrderForm";
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React Redux</h1>
        </header>
        <div className="App-intro">
          <OrderForm />
          {/* <List />
          <Form /> */}
        </div>
      </div>
    );
  }
}

export default App;
