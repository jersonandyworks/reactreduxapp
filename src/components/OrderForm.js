import React, { Component } from "react";
import { connect } from "react-redux";
import Aux from "../components/auxContainer";
import * as actionDispatch from "../redux/actions/orderAction";
class OrderForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: ""
    };
  }

  onChangeHandler = event => {
    const states = { ...this.state };
    states[event.target.name] = event.target.value;
    this.setState(states);
  };

  onSubmitHandler = () => {
    this.props.newOrder({
      id: Math.random()
        .toString(36)
        .substring(7),
      name: this.state.name
    });
  };

  onDeleteHandler = id => {
    console.log("deleting", id);
    this.props.deleteOrder(id);
  };

  render() {
    const orders = Object.keys(this.props.orders).map(key => {
      return (
        <li
          key={this.props.orders[key].id}
          onClick={() => this.onDeleteHandler(this.props.orders[key].id)}
        >
          {this.props.orders[key].name}
        </li>
      );
    });
    return (
      <Aux>
        <ul>{orders}</ul>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            className="form-control"
            name="name"
            value={this.state.name}
            onChange={event => this.onChangeHandler(event)}
          />
        </div>
        <button
          type="submit"
          onClick={this.onSubmitHandler}
          className="btn btn-success btn-lg"
        >
          SAVE
        </button>
      </Aux>
    );
  }
}

const mapStateToProps = state => {
  return { orders: state.order.orders };
};

const mapDispatchToProps = dispatch => {
  return {
    newOrder: order => {
      console.log("FUCKED!");
      dispatch(actionDispatch.addOrder(order));
    },
    deleteOrder: id => {
      dispatch(actionDispatch.removeOrder(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderForm);
