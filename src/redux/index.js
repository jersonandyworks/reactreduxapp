import React from "react";
import { render } from "react-dom";
import store from "./store/index";
import { addArticle } from "./actions/index";

window.store = store;
window.addArticle = addArticle;
