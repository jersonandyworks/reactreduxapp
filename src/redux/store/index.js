import { createStore, combineReducers } from "redux";
import rootReducer from "../reducers/index";
import orderReducer from "../reducers/orderReducer";
const combinedReducers = combineReducers({
  article: rootReducer,
  order: orderReducer
});

const store = createStore(combinedReducers);

export default store;
