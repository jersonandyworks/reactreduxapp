import * as actionType from "../constants/action-types";

export const addOrder = order => ({
  type: actionType.ADD_ORDER,
  payload: order
});

export const removeOrder = id => ({
  type: actionType.REMOVE_ORDER,
  payload: id
});
