import { ADD_ORDER, REMOVE_ORDER } from "../constants/action-types";

// configure intial state
const initialState = {
  orders: []
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ORDER:
      console.log(state.orders);
      return {
        ...state,
        orders: state.orders.concat({
          id: action.payload.id,
          name: action.payload.name
        })
      };
    case REMOVE_ORDER:
      //Make new array

      const newArray = state.orders.filter(order => {
        return order.id !== action.payload;
      });
      return { ...state, orders: newArray };
    default:
      return state;
  }
};

export default orderReducer;
